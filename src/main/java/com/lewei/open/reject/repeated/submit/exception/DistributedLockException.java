package com.lewei.open.reject.repeated.submit.exception;

import com.lewei.open.distributed.lock.core.DistributedLock;

/**
 * {@link  DistributedLock} initialize {@link RuntimeException}
 *
 * @author <a href="mailto:yins_emial@foxmail.com">yins</a>
 * @date 2021-11-08 23:45
 */
public class DistributedLockException extends RuntimeException {


    public DistributedLockException(Throwable cause) {
        super("initialize bean(com.lewei.open.distributed.lock.core.DistributedLock) error", cause);
    }
}
