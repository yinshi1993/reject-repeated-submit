package com.lewei.open.reject.repeated.submit.type;


import com.lewei.open.reject.repeated.submit.annotation.RejectRepeatedSubmit;

/**
 * {@link RejectRepeatedSubmit} request method type
 *
 * @author <a href="mailto:yins_emial@foxmail.com">yins</a>
 * @date 2021-11-08 18:29
 */
public enum RequestType {

    /**
     * 该方法不支持重复请求
     * 显示设置，或者无参数列表方法
     */
    METHOD,
    /**
     * Default
     * 对应数据唯一标识不支持重复请求
     */
    UNIQUE
}
