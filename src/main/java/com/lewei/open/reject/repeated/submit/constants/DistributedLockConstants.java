package com.lewei.open.reject.repeated.submit.constants;

/**
 * @author yins
 * @date 2021年11月08日 20:58
 * @since 1.0.0
 */
public interface DistributedLockConstants {

    String REDIS_DISTRIBUTED_LOCK =  "com.lewei.open.distributed.lock.redis.RedisDistributedLock";

    String ZOOKEEPER_DISTRIBUTED_LOCK =  "com.lewei.open.distributed.lock.zk.ZookeeperDistributedLock";

}
