package com.lewei.open.reject.repeated.submit.exception;

/**
 * Annotation usage error {@link RuntimeException}
 *
 * @author <a href="mailto:yins_emial@foxmail.com">yins</a>
 * @date 2021-11-08 23:45
 */
public class AnnotationUseException extends RuntimeException {

    public AnnotationUseException() {
        super("Annotation usage error");
    }
}
