package com.lewei.open.reject.repeated.submit.annotation;

import com.lewei.open.reject.repeated.submit.type.RequestType;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * reject repeated submit annotation
 *
 * @author <a href="mailto:yins_emial@foxmail.com">yins</a>
 * @date 2021-11-08 18:27
 */
@Retention(RUNTIME)
@Target(METHOD)
@Documented
@Inherited
public @interface RejectRepeatedSubmit {

    /**
     * 参数唯一标识字段
     */
    String filed() default "id";

    /**
     * 唯一标识字段参数列表中所处下标
     */
    int index() default 0;

    /**
     * 分布式锁自动过期时长
     */
    long duration() default 3;

    /**
     * 时长单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 当分布式锁被占用是，尝试获取次数
     */
    int retry() default 1;

    /**
     * 请求类型
     * 基于参数的分布式锁还是基于方法
     *
     * @see {@link RequestType}
     */
    RequestType type() default RequestType.UNIQUE;

}
