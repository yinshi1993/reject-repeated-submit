# 接口(方法)防重复提交

与 [分布式锁](https://gitee.com/yinshi1993/distributed-lock) 配和使用

## 注解说明
```java
@Retention(RUNTIME)
@Target(METHOD)
@Documented
@Inherited
public @interface RejectRepeatedSubmit {

    /**
     * 参数唯一标识字段
     */
    String filed() default "id";

    /**
     * 唯一标识字段参数列表中所处下标
     */
    int index() default 0;

    /**
     * 分布式锁自动过期时长
     */
    long duration() default 3;

    /**
     * 时长单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 当分布式锁被占用是，尝试获取次数
     */
    int retry() default 1;

    /**
     * 请求类型
     * 基于参数的分布式锁还是基于方法
     *
     * @see {@link RequestType}
     */
    RequestType type() default RequestType.UNIQUE;

}
```
```java
@RestController
public class TestController {

    @RequestMapping("/noArgs")
    @RejectRepeatedSubmit
    public ResponseEntity<String> ok() throws InterruptedException {
        Thread.sleep(1000);
        return ResponseEntity.ok("ok");
    }

    @RequestMapping("/path/{id}")
    @RejectRepeatedSubmit
    public ResponseEntity<String> path(@PathVariable("id") String id) throws InterruptedException {
        Thread.sleep(1000);
        return ResponseEntity.ok("ok");
    }


    @RequestMapping("/object")
    @RejectRepeatedSubmit
    public ResponseEntity<String> object(Map<String, String> map) throws InterruptedException {
        Thread.sleep(1000);
        return ResponseEntity.ok("ok");
    }



}
```